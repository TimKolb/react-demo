import React from 'react';
import {AppRegistry, Pano, Text, View} from 'react-vr';

class WelcomeToVR extends React.Component {
    render() {
        // Displays "hello" text on top of a loaded 360 panorama image.
        // Text is 0.8 meters in size and is centered three meters in front of you.
        return (
            <View>
                <Pano source={asset('chess-world.jpg')}/>
                <Text
                    style={{
                        fontSize: 0.8,
                        layoutOrigin: [0.5, 0.5],
                        transform: [{translate: [0, 0, -3]}],
                    }}>
                    hello
                </Text>
            </View>
        );
    }
};

AppRegistry.registerComponent('WelcomeToVR', () => WelcomeToVR);