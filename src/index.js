import React from 'react';
import ReactDOM from 'react-dom';
import BeerButton from './components/BeerButton';
import {Provider} from 'react-redux';
import store from './store';
import './index.css';

ReactDOM.render(
    <Provider store={store}><BeerButton /></Provider>,
    document.getElementById('root')
);
