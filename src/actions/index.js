export const DRINK_BEER = 'DRINK_BEER';

export default function drinkBeer() {
    return {
        type: DRINK_BEER
    }
}