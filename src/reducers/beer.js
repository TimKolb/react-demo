import {DRINK_BEER} from '../actions'

const initialState = {
    count: 0
};

export function beer(state = initialState, action) {
    switch (action.type) {
        case DRINK_BEER:
            return {
                ...state,
                count: state.count + 1
            };
        default:
            return state;
    }
}