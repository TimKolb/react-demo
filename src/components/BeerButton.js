import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import drinkBeer from '../actions';
//import Clock from './Clock';

const styles = {
    description: {
        border: '1px solid red',
        display: 'inline-block'
    }
};

export class BeerButton extends Component {
    static propTypes = {
        count: PropTypes.number.isRequired,
        dispatch: PropTypes.func.isRequired
    };

    handleClick = () => {
        const {dispatch} = this.props;
        dispatch(drinkBeer());
    };

    render() {
        const {count} = this.props;
        return (
            <div>
                <p style={styles.description}>You hattest {count} Bier</p>
                <button onClick={this.handleClick}>Trink</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    count: state.beer.count
});

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BeerButton);