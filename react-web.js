import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class HelloWorldApp extends Component {
    render() {
        return (
            <Text>Hello world!</Text>
        );
    }
}

ReactDOM.render(
    <HelloWorldApp />,
    document.getElementById('root')
);
